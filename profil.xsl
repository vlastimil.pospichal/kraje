<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output type="html" omit-xml-declaration="yes" indent="yes"/>
<xsl:strip-space elements="*"/>

<xsl:template match="/person">
    <html>
        <body>
            <ul>
                <li>Jméno: <xsl:value-of select="jmeno"/></li>
                <li>Příjmení: <xsl:value-of select="prijmeni"/></li>
                <xsl:apply-templates select="narozen"/>
                <xsl:apply-templates select="kraj"/>
            </ul>
        </body>
    </html>
</xsl:template>

<xsl:template match="kraj">
    <xsl:variable name="kraj" select="."/>
    <li>Kraj: <xsl:value-of
            select="document('kraje.xml')/kraje/kraj[@id=$kraj]/text()"/>
    </li>
</xsl:template>

<xsl:template match="narozen">
    <xsl:variable name="den" select="substring-before(., '.')"/>
    <xsl:variable name="mesicRok" select="substring-after(., '.')"/>
    <xsl:variable name="mesic" select="substring-before($mesicRok, '.')"/>
    <xsl:variable name="rok" select="substring-after($mesicRok, '.')"/>
    <li>Narozen: <xsl:value-of select="$den"/>. <xsl:value-of
            select="document('mesice.xml')/mesice/item[@id=$mesic]/text()"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="$rok"/>
    </li>
</xsl:template>

</xsl:stylesheet>
